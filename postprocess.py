#!/usr/bin/env python3
import re
import sys
from typing import Callable, Pattern, Match
import urllib.request


IMPORT_PATTERN: Pattern = re.compile('''
    @import \s+
    url \s*
    \( \s*
    (?:
        "   ( (?: https?: )? //[^"]+ ) "
    |   '   ( (?: https?: )? //[^']+ ) '
    |       ( (?: https?: )? //[^)]+ )
    )
    \s* \)
    \s* ;
''', re.VERBOSE)


def expand_import_url(match: Match) -> str:
    url: str = match.group(1) or match.group(2) or match.group(3)
    with urllib.request.urlopen(url) as u:
        encoding = 'utf-8'
        try:
            content_type = u.headers['content-type']
        except KeyError:
            pass
        else:
            charset_match = re.search('charset=(\S+)', content_type)
            if charset_match:
                encoding = charset_match.group(1)
        return u.read().decode(encoding)


def expand_imports(css: str) -> str:
    css = IMPORT_PATTERN.sub(expand_import_url, css)
    if IMPORT_PATTERN.search(css):
        css = expand_imports(css)
    return css


def main():
    css = sys.stdin.read()
    print(expand_imports(css))


if __name__ == '__main__':
    main()
